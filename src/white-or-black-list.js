module.exports = function(RED) {
  function Filter(config) {
      RED.nodes.createNode(this, config);
      let node = this;
      const nodeReadyStatus = {
        fill: "green",
        shape: "dot",
        text: "ready"
      }
      node.status(nodeReadyStatus);
      node.on('input', function(msg) {
        if (!Array.isArray(msg.payload)) {
          node.status({
            fill: "red",
            shape: "dot",
            text: "wrong-format"
          });      
          return;
        }
        let detected = [];
        if (config.mode === "listed") {
          detected = msg.payload.filter(item => {
            return typeof(item) === "object" && config.list.find(({ name }) => {
              return item.name === name
            })
          })
        } else {
          detected = msg.payload.filter(item => {
            return typeof(item) === "object" && !config.list.find(({ name }) => {
              return item.name === name
            })
          })
        }
        
        if (detected.length > 0) {
          let sendData = {
            payload: {
              detected: detected,
            }
          }
          if(msg.image) {
            sendData.image = msg.image
          }
          node.send(sendData);
          let names = detected.map(item => item.name).join();
          node.status({
            fill: "green",
            shape: "dot",
            text: names
          });
        } 


        // if(detected.length > 0) {
        //   node.send([
        //     {
        //       payload: detected
        //     },
        //     {
        //       payload: notFounds
        //     },
        //   ]);
        // }
      });
  }
  RED.nodes.registerType("white-or-black-list", Filter);
}