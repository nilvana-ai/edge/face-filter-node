# Face filter node

## Get Started

```shell
npm install --save https://gitlab.com/nilvana-ai/edge/face-filter-node.git
```

## Copyright and license

Copyright inwinSTACK Inc. under [the Apache 2.0 license](LICENSE.md).
